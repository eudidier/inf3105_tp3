/*  INF3105 - Structures de données et algorithmes
 *  UQAM / Département d'informatique
 *  Automne 2015 / TP3
 *
 *  Henri-Joël HAZOUNME + HAZH12079009
 *  Euloge Nihezagire + NIHE16098601
 */

#include <cstdio>
#include <limits>
#include <math.h>
#include <queue>
#include <sstream>
#include "carte.h"
#include <vector>
#include <set>


using namespace std;
int extractMax(map<int, int>& arreteMax, vector<int> precedent);

int extractMinPrecedent(vector<int>& precedent, set<int>& noeudTraiter);

void Carte::ajouterSommet(const Coordonnee& s, int site) {
    this->noeuds[site].coordo = s;

}

void Carte::ajouterAreteNonOrientee(string rue, int flux, int site, int site1) {

    noeuds[site].voisins[site1].rue = rue;
    noeuds[site].voisins[site1].flux = flux;
    noeuds[site1].voisins[site].rue = rue;
    noeuds[site1].voisins[site].flux = flux;

}

void Carte::prim() {

    int neoudActuel = 0;
    vector<int> precedent;
    map<int, int> adjance;
    map<int, int> copieAdjance;
    set<int> noeudTraiter;

    for (auto itCarte = noeuds.cbegin(); itCarte != noeuds.cend(); ++itCarte) {
        precedent.push_back(0);
        adjance[itCarte->first] = 0;
    }
    neoudActuel = extractMax(adjance, precedent);
    int somme = 0;
    copieAdjance = adjance;
    while (!adjance.empty()) {

        int neoudActuel = extractMax(adjance, precedent);

        for (auto itAdj = noeuds[neoudActuel].voisins.cbegin(); itAdj !=
                noeuds[neoudActuel].voisins.cend(); ++itAdj) {

            if (adjance.find(itAdj->first) != adjance.end() &&
                    adjance[itAdj->first] <
                    noeuds[neoudActuel].voisins[itAdj->first].flux) {

                adjance[itAdj->first] =
                        noeuds[neoudActuel].voisins[itAdj->first].flux;
                copieAdjance[itAdj->first] =
                        noeuds[neoudActuel].voisins[itAdj->first].flux;
                precedent[itAdj->first] = neoudActuel;

            }
        }
    }
    for (int i = 2; i <= precedent.size(); i++) {
        int noeudPrecedent = extractMinPrecedent(precedent, noeudTraiter);
        cout << "n" << precedent[noeudPrecedent] << " n" << noeudPrecedent << endl;
        cout << noeuds[precedent[noeudPrecedent]].voisins[noeudPrecedent].rue << endl;
        cout << noeuds[precedent[noeudPrecedent]].voisins[noeudPrecedent].flux << endl;
    }
    for (auto it = copieAdjance.cbegin(); it != copieAdjance.cend(); ++it) {
        somme += copieAdjance[it->first];
    }
    cout << "---\n" << somme << endl;
}

int extractMax(map<int, int>& arreteMax, vector<int> precedent) {
    int max = 0, noeudMax = 1;
    for (auto itAdjance = arreteMax.cbegin(); itAdjance != arreteMax.cend();
            ++itAdjance) {
        if (itAdjance->second > max) {
            max = itAdjance->second;
            noeudMax = itAdjance->first;
        }
        else
        if (itAdjance->second == max) {
            if(precedent[itAdjance->first] < precedent[noeudMax]){
            noeudMax = itAdjance->first;
            }
        }
    }
    arreteMax.erase(noeudMax);
    return noeudMax;
}

int extractMinPrecedent(vector<int>& precedent, set<int>& noeudTraiter) {
    int min = precedent.size(), precedentIndice = 0;
    for (int i = 2; i <= precedent.size(); i++) {
        if (precedent[i] < min && *noeudTraiter.find(i) != i) {
            min = precedent[i];
            precedentIndice = i;
        }
    }
    noeudTraiter.insert(precedentIndice);
    return precedentIndice;
}

istream& operator>>(istream& is, Carte& carte) {
    string site, site1;
    string rue;
    string flux;
    Coordonnee coor;
    is >> site >> coor;
    carte.ajouterSommet(coor, atoi(site.substr(1).c_str()));
    while (site != "---") {

        carte.ajouterSommet(coor, atoi(site.substr(1).c_str()));
        is >> site;
        if (site == "---")
            break;
        is >> coor;
    }
    is >> rue;

    while (!is.eof()) {

        std::queue<string> first;
        while (rue != ";") {
            first.push(rue);
            is >> rue;
        }
        rue = first.front();
        first.pop();
        first.pop();
        flux = first.back();
        site = first.front();
        first.pop();
        site1 = first.front();
        while (site1 != flux) {
            carte.ajouterAreteNonOrientee(rue, atoi(flux.c_str())
                    , atoi(site.substr(1).c_str()), atoi(site1.substr(1).c_str()));
            site = site1;
            first.pop();
            site1 = first.front();
        }
        first.pop();
        is >> rue;
    }
    return is;
}

