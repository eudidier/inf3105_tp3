/*  INF3105 - Structures de données et algorithmes
 *  UQAM / Département d'informatique
 *  Automne 2015/ TP3
 *
 *  Henri-Joël HAZOUNME + HAZH12079009
 *  Euloge Nihezagire + NIHE16098601
 */

#if !defined(_CARTE__H_)
#define _CARTE__H_
#include "coordonnee.h"
#include <cassert>
#include <istream>
#include <list>
#include <map>
#include <string>
#include <vector>

using namespace std;

class Carte {
public:

    void ajouterSommet(const Coordonnee& s, int site);
    void ajouterAreteNonOrientee(string rue, int flux, int site, int site1);
    void prim();

private:

    struct Arret {
        string rue;
        int flux;
    };

    struct Noeud {
        map<int, Arret> voisins;
        Coordonnee coordo;
    };

    map<int, Noeud> noeuds;

    friend istream& operator>>(istream& is, Carte& carte);
};

#endif

