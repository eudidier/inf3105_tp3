//
//#include <iostream>
//#include <vector>
//
//int main()
//{
//	std::vector<int> myvector;
//
//	// set some content in the vector:
//	for (int i = 0; i<100; i++) myvector.push_back(i);
//
//	std::cout << "size: " << myvector.size() << "\n";
//	std::cout << "capacity: " << myvector.capacity() << "\n";
//	std::cout << "max_size: " << myvector.max_size() << "\n";
//	return 0;
//}

//#include <list> 
//#include <iostream> 
//
//int main() {
//	std::list<int> ma_liste;
//	ma_liste.push_back(4);
//	ma_liste.push_back(5);
//	ma_liste.push_back(4);
//	ma_liste.push_back(1);
//	std::list<int>::const_iterator
//		lit(ma_liste.begin()),
//		lend(ma_liste.end());
//	for (; lit != lend; ++lit) std::cout << *lit << ' ';
//	std::cout << std::endl;
//	return 0;
//}

//#include <vector> 
//#include <iostream> 
//
//int main() {
//	std::vector<int> mon_vecteur;
//	mon_vecteur.push_back(4);
//	mon_vecteur.push_back(2);
//	mon_vecteur.push_back(5);
//
//	// Pour parcourir un vector (même const) on peut utiliser les iterators ou les index 
//	for (int i = 0; i<mon_vecteur.size(); ++i) {
//		std::cout << mon_vecteur[i] << ' ';
//	}
//	std::cout << std::endl;
//
//	std::vector<int> v(5, 69); // crée le vecteur 69,69,69,69,69 
//	for (int i = 0; i<v.size(); ++i) {
//		std::cout << v[i] << ' ';
//	}
//	std::cout << std::endl;
//	v[0] = 5;
//	v[1] = 3;
//	v[2] = 7;
//	v[3] = 4;
//	v[4] = 8;
//	for (int i = 0; i<v.size(); ++i) {
//		std::cout << v[i] << ' ';
//	}
//	return 0;
//}

//#include <set> 
//#include <iostream> 
//
//int main() {
//	std::set<int> s; 
//	s.insert(2); // s contient 2 
//	s.insert(5); // s contient 2 5 
//	s.insert(2); // le doublon n'est pas inséré 
//	s.insert(1); // s contient 1 2 5 
//	std::set<int>::const_iterator
//		sit(s.begin()),
//		send(s.end());
//	for (; sit != send; ++sit) std::cout << *sit << ' ';
//	std::cout << std::endl;
//	return 0;
//}

//#include <map> 
//#include <string> 
//#include <iostream> 
//
//int main() {
//	std::map<std::string, int> map_mois_idx;
//	map_mois_idx["janvier"] = 1;
//	map_mois_idx["février"] = 2;
//	//... 
//	std::map<std::string, int>::const_iterator
//		mit(map_mois_idx.begin()),
//		mend(map_mois_idx.end());
//	for (; mit != mend; ++mit) {
//		std::cout << mit->first << '\t' << mit->second << std::endl;
//	}
//	return 0;
//}

//#include <list> 
//#include <iostream> 
//
//const std::list<int> creer_liste() {
//	std::list<int> l;
//	l.push_back(3);
//	l.push_back(4);
//	return l;
//}
//
//int main() {
//	const std::list<int> ma_liste(creer_liste());
//
//	/*
//	// ne compile pas car l est const
//	std::list<int>::iterator
//	lit1 (l.begin()),
//	lend1(l.end());
//	for(;lit1!=lend1;++lit1) std::cout << *lit1 << ' ';
//	std::cout << std::endl;  */
//
//	std::list<int>::const_iterator
//		lit2(ma_liste.begin()),
//		lend2(ma_liste.end());
//	for (; lit2 != lend2; ++lit2) std::cout << *lit2 << ' ';
//	std::cout << std::endl;
//
//	return 0;
//}

//#include <set> 
//#include <iostream> 
//
//int main(){
//	std::set<int> s;
//	s.insert(1); // s = {1} 
//	s.insert(4); // s = {1, 4} 
//	s.insert(3); // s = {1, 3, 4} 
//	std::set<int>::const_reverse_iterator
//		sit(s.rbegin()),
//		send(s.rend());
//	for (; sit != send; ++sit) std::cout << *sit << std::endl;
//	return 0;
//}

//
//#include <vector> 
//#include <algorithm>
//#include <iostream>
//#include <iterator>
//#include <list>
//#include <string>
//
//using namespace std;
//
//int cpt = 0;
//void charToUpper(char & c) {
//	if (c >= 'a' && c <= 'z') {
//		c &= 223;
//	}
//}
//
//	void StringToUpper(string & str) {
//		cpt++;
//		str[0] = cpt + 0x30;
//		for_each(str.begin(), str.end(), charToUpper);
//	}
//
//int main() {
//
//	list<string> maListe;
//	maListe.push_front("x_C++");
//	maListe.push_front("x_Python");
//	maListe.push_back("x_Java");
//	maListe.push_back("x_C Sharp");
//
//	for_each(maListe.begin(), maListe.end(), StringToUpper);
//		std::list<string>::const_iterator
//			lit2(maListe.begin()),
//			lend2(maListe.end());
//		for (; lit2 != lend2; ++lit2) std::cout << *lit2 << ' ';
//		std::cout << std::endl;
//
//	return 0;
//}


//#include <iostream>
//#include <string>
//#include <vector>
// 
//using namespace std;
// 
//int main(){
//    vector<string> tab;
//    tab.push_back("un"); // tab.ajouter("un");
//    tab.push_back("deux");
//    tab.push_back("trois");
//    tab.push_back("quatre");
//    tab.push_back("cinq");
//    
//    vector<string> tab2 = tab;
//    for(vector<string>::iterator iter=tab2.begin();iter!=tab2.end();++iter)
//       std::cout << *iter << std::endl;
//}


//#include <iostream>
//#include <string>
//#include <vector>
//#include <algorithm>
// 
//using namespace std;
// 
//int main(){
//    vector<string> tab;
//    tab.push_back("un"); // tab.ajouter("un");
//    tab.push_back("deux");
//    tab.push_back("trois");
//    tab.push_back("quatre");
//    tab.push_back("cinq");
//    
//    sort(tab.begin(), tab.end());
//    for(vector<string>::iterator iter=tab.begin();iter!=tab.end();++iter)
//       std::cout << *iter << std::endl;
// 
//    return 0;
//}


//#include <iostream>
//#include <string>
//#include <set>
//#include <algorithm>
//using namespace std;
// 
//int main(){
//    set<string> ensemble;
//    ensemble.insert("un");
//    ensemble.insert("deux");
//    ensemble.insert("trois");
//    ensemble.insert("quatre");
//    ensemble.insert("cinq");
//    
//    for(set<string>::iterator iter=ensemble.begin();iter!=ensemble.end();++iter)
//       std::cout << *iter << std::endl;
// 
//    return 0;
//}


//#include <iostream>
//#include <string>
//#include <map>
// 
//using namespace std;
// 
//int main(){
//    map<int, string> dictionnaire;
//    dictionnaire[1] = "un";
//    dictionnaire[2] = "deux";
//    dictionnaire[3] = "trois";
//    dictionnaire[4] = "quatre";
//    dictionnaire[5] = "cinq";
//    
//    cout << "dictionnaire[5]=" << dictionnaire[5] << std::endl;
//    cout << "dictionnaire[1]=" << dictionnaire[1] << std::endl;
//    cout << "dictionnaire[3]=" << dictionnaire[3] << std::endl;
//    cout << "dictionnaire[2]=" << dictionnaire[2] << std::endl;
//    cout << "dictionnaire[4]=" << dictionnaire[4] << std::endl;
//    
//    cout << "---" << std::endl;
//    for(map<int, string>::iterator iter=dictionnaire.begin();iter!=dictionnaire.end();++iter){
//        cout << "dictionnaire[" << iter->first << "]=" << iter->second << std::endl;
//    }
// 
//    return 0;
//}
